<?php
namespace app\controllers;

use app\core\Controller;
use app\core\View;
use app\models\ProductsModel;
use app\core\Database;

class ProductList extends Controller
{
    public function index()
    {
        $this->view = new View;
        $this->model = new ProductsModel;
        $data = $this->model->getData();
        $this->view->generate('list', $data);
    }

    public function massDelete()
    {
        $this->model = new ProductsModel;
        $this->model->massDelete();
        $this->index();
    }
}