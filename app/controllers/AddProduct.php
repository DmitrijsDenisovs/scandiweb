<?php
namespace app\controllers;
use app\core\Controller;
use app\core\View;
use app\models\ProductsModel;

class AddProduct extends Controller
{
    public function index()
    {
        $this->view = new View;
        $this->view->generate('addPage');
    }

    public function insertProduct()
    {
        $this->model = new ProductsModel;
        $this->model->addProduct();

        $data = $this->model->getData();
        $this->view = new View;
        $this->view->generate('list', $data);
    }
}