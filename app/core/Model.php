<?php
namespace app\core;
abstract class Model
{
    abstract public function getData();
}