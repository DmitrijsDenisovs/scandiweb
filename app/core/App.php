<?php
namespace app\core;
class App
{
    protected $controller = 'ProductList';
    protected $method = 'index';
    protected $parameters = [];

    public function __construct()
	{
        $url = $this->parseUrl();
        if ($url[0] == 'addproduct') {
		    $url[0] = 'AddProduct';
		}
		if (file_exists('../app/controllers/' . $url[0] . '.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        }	
		
		$this->controller = 'app\\controllers\\' . $this->controller;
		$this->controller = new $this->controller;

		if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

		$this->parameters = $url ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->method], $this->parameters); 
	}

    protected function parseUrl()
	{
        if (isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}