<?php
namespace app\core;
use PDO;
use PDOException;
use app\productClasses\Book;
use app\productClasses\DVDDisc;
use app\productClasses\Furniture;
class Database
{  
    private const DB_PASS = '<F>kkVwk|fM?Jf<6';
    private const DB_USER = 'id17412987_root';
    private const DB_NAME = 'id17412987_scandiweb_task';
    private const DB_HOST = 'localhost';
    private const DB_PORT = '3306';
    private const TB_NAME = 'product';  
    private const PRIMARY_KEY = 'sku';

    private $connection;

    public function __construct() 
    {
        $dsn = 'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . ';port=' . self::DB_PORT .';charset=utf8mb4';
        try {
            $this->connection = new PDO($dsn, self::DB_USER, self::DB_PASS); 
        } catch (PDOException $e) {
            $this->connection = null;
        }
    }

    public function selectAll()
    {
        if ($this->connection != null) {
            $query = 'SELECT * FROM ' . self::TB_NAME . ' ORDER BY ' . self::PRIMARY_KEY . ' ASC';
            $statement = $this->connection->prepare($query);
            $statement->execute();
            $queryResult = $statement->fetchAll();
            return $queryResult;
        }
        return null;
    }

    public function add($item)
    {
        if ($this->connection != null) {
            $query = 'INSERT INTO ' . self::TB_NAME .'(' . 
                $item->getColumnNames() . ') VALUES (' . $item->getValuePlaceHolders() . ')';
            $values = $item->getValues();
            $statement = $this->connection->prepare($query);
            $statement->execute($values);
        }
    }

    public function delete($primaryKeyValue)
    {
        if ($this->connection != null) {
            $query = 'DELETE FROM ' . self::TB_NAME . ' WHERE ' . self::PRIMARY_KEY . ' = :primaryKeyValue';
            $statement = $this->connection->prepare($query);
            $statement->bindParam(':primaryKeyValue', $primaryKeyValue);
            $statement->execute();

        }
    }

    public function __destruct()
    {
        $this->connection = null;
    }

}
