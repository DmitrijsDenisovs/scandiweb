<?php
namespace app\core;
class View
{
    function generate($templateView, $data = null)
    {
        if (file_exists('../app/views/' . $templateView . '.php')) {
            include '../app/views/header.php';
            include '../app/views/' . $templateView . '.php';
            include '../app/views/footer.php';
        }
    }
}