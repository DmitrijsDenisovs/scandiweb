<?php
namespace app\models;
use app\core\Model;
use app\core\Database;

class ProductsModel extends Model
{

    public function getData()
    { 
        $database = new Database;
        $rawData = $database->selectAll();
        $data = [];
        foreach ($rawData as $row) {
            
            $sku = $row['sku'];
            $name = $row['name'];
            $priceUsd = $row['price_usd'];
            $class = $row['class'];
            $size = $row['size'];
            $weight = $row['weight'];
            $height = $row['height'];
            $width = $row['width'];
            $length = $row['length'];

            $className = 'app\\productClasses\\' . $class;
            $item = new  $className($sku, $name, $priceUsd, $size, $weight, $height, $width, $length);
            array_push($data, $item);
        }
        return $data;
    }

    public function massDelete()
    {
        $database = new Database;
        if (isset($_POST['itemsToDelete'])) {
            $itemsToDelete = $_POST['itemsToDelete'];
            foreach ($itemsToDelete as $itemId) {
                $database->delete($itemId);
            }
            unset($_POST['itemsToDelete']);
        }
    }

    public function addProduct()
    {
        $class = $_POST['productType'];
        unset($_POST['productType']);
        $classNameSpace = 'app\\productClasses\\' . $class;
        $newItem = $classNameSpace::createItem();
        $database = new Database;
        $database->add($newItem);
    }
}