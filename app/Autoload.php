<?php
spl_autoload_register(function ($className){
    $prefix = '..';
    $className = str_replace('\\', '/', $className);
    $namespaces = explode('/', $className);
    $filePath = $prefix;
    foreach ($namespaces as $namespace) {
        $filePath = $filePath . '/' . $namespace ;
    }
    $filePath = $filePath . '.php';
    if (file_exists($filePath)) {
        require_once $filePath;
    }
    else {
        return false;
    }
});
