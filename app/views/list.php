
</head>
<body>
    <form action="<?= ROOT_DIRECTORY ?>/ProductList/massDelete" method="POST">
    <div class="border-bottom m-3">
        <div class="d-grid border-bottom p-2">
            <div class="row">
                <div class="col-lg-10 col-md-7 col-sm-6 col-xs-6 col-6 heading-div">
                    <h3>Product List</h3>
                </div>
                <div class="col">
                    <a href="<?= ROOT_DIRECTORY ?>/addproduct" class="btn btn-outline-primary">ADD</a>
                    <button type="submit" href="" class="btn btn-outline-danger" id="delete-product-btn">MASS DELETE</button>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap justify-content-center">
            <?php 
                if($data != null):
                    foreach($data as $item):
            ?>
                <div class="item">
                    <input class="delete-checkbox" type="checkbox" name="itemsToDelete[]" value="<?= $item->getSku() ?>">
                        <p>
                            <?= $item->getSku()?> 
                        </p>
                        <p> 
                            <?= $item->getName()?>
                        </p>
                        <p>
                            <?= $item->getPriceUsd()?> $
                        </p>
                        <p>
                            <?= $item->getAttributeName()?>: <?= $item->getAttributeValue()?> <?= $item->getAttributeUnit() ?>
                        </p>
                </div>    
            <?php 
                    endforeach;
                endif;
            ?>    
        </div>
        </form>
    </div>