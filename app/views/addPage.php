
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js"></script>
	<script src="<?= ROOT_DIRECTORY ?>/js/script.js"></script>
</head>
<body>
    <form action="<?= ROOT_DIRECTORY ?>/AddProduct/insertProduct" method="POST" class="form-horizontal" 
        id="product_form">
    <div class="border-bottom m-3">
        <div class="d-grid border-bottom p-2">
            <div class="row">
                <div class="col-lg-10 col-md-7 col-sm-6 col-xs-6 col-6 heading-div">
                    <h3>Product Add</h3>
                </div>
                <div class="col"> 
                    <button type="submit" class="btn btn-outline-primary">Save</button>
                    <a class="btn btn-outline-danger" href="<?= ROOT_DIRECTORY ?>/ProductList">Cancel</a>
                </div>
            </div>
        </div>
        <div class="d-grid">

            <div class="row m-2">
                <div class="col-2">
                    <label for="sku"><h5>SKU</h5></label>  
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="sku" name="sku">
                </div> 
            </div>

            <div class="row m-2">
                <div class="col-2">
                    <label for="name"><h5>Name</h5></label>  
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="name" name="name">
                </div> 
            </div>

            <div class="row m-2">
                <div class="col-2">
                    <label for="price"><h5>Price ($) </h5></label>  
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="price" name="price">
                </div> 
            </div>

            <div class="row m-2">
                <div class="col-2">
                    <label for="productType"><h6>Type Switcher</h6></label>  
                </div>
                <div class="col">
                    <select name="productType" class="form-control" id="productType">
                        <option value="DVDDisc">DVD</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                </div> 
            </div>
            <div class="row m-2 DVDDisc">
                    <div class="col-2">
                        <label for="size"><h5>Size (MB)</h5></label>  
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="size" name="size">
                        <p>Please provide size in MB</p>
                    </div> 
            </div>
            <div class="row m-2 Furniture">
                    <div class="col-2">
                        <label for="height"><h5>Height (CM)</h5></label>  
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="height" name="height">
                    </div> 
                </div>
                <div class="row m-2 Furniture">
                    <div class="col-2">
                        <label for="width"><h5>Width (CM)</h5></label>  
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="width" name="width">
                    </div> 
                </div>
                <div class="row m-2 Furniture">
                    <div class="col-2">
                        <label for="length"><h5>Length (CM)</h5></label>  
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="length" name="length">
                        <p>Please provide dimensions in CM</p>
                    </div> 
                </div>
                <div class="row m-2 Book">
                    <div class="col-2">
                        <label for="size"><h5>Weight (KG)</h5></label>  
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="weight" name="weight">
                        <p>Please provide weight in KG</p>
                    </div> 
                </div>
        </div>
    </div>
</form>