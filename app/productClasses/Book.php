<?php
namespace app\productClasses;
use app\productClasses\ProductItem;

class Book extends ProductItem
{
    private const ATTRIBUTE_NAME = 'Weight';
    private const ATTRIBUTE_UNIT = 'KG';
    private $weight;

    public function __construct($sku, $name, $priceUsd, $size, $weight, $height, $width, $length)
    {
        parent::__construct($sku, $name, $priceUsd);
        $this->setWeight($weight);
    }
    static function createItem(){
        $newItem = new Book($_POST['sku'], $_POST['name'], $_POST['price'], null, $_POST['weight'], null, null, null);
        unset($_POST['sku']);
        unset($_POST['name']);
        unset($_POST['price']);
        unset($_POST['weight']);
        return $newItem;
    }

    public function getAttributeName()
    {
        return self::ATTRIBUTE_NAME;
    }

    public function getAttributeValue()
    {
        return $this->getWeight();
    }

    public function getSpecificColumnNames()
    {
        $specificColumnNames = 'weight';
        return $specificColumnNames;
    }

    public function getSpecificValues()
    {
        $specificValueArray = array('Book' ,$this->getWeight());
        return $specificValueArray;
    }
    public function getSpecificValuePlaceHolders()
    {
        $specificValuePlaceHolders = '?, ?';
        return $specificValuePlaceHolders;
    }

    public function getAttributeUnit()
    {
        return self::ATTRIBUTE_UNIT;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }
}