<?php
namespace app\productClasses;
use app\productClasses\ProductItem;

class DVDDisc extends ProductItem
{
    private const ATTRIBUTE_NAME = 'Size';
    private const ATTRIBUTE_UNIT = 'MB';
    private $size;

    public function __construct($sku, $name, $priceUsd, $size, $weight, $height, $width, $length)
    {
        parent::__construct($sku, $name, $priceUsd);
        $this->setSize($size);
    }
    static function createItem(){
        $newItem = new DVDDisc($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['size'], null, null, null, null);
        unset($_POST['sku']);
        unset($_POST['name']);
        unset($_POST['price']);
        unset($_POST['size']);
        return $newItem;
    }
    public function getAttributeName()
    {
        return self::ATTRIBUTE_NAME;
    }

    public function getAttributeValue()
    {
        return $this->getSize();
    }

    public function getSpecificColumnNames()
    {
        $specificColumnNames = 'size';
        return $specificColumnNames;
    }
    public function getSpecificValuePlaceHolders()
    {
        $specificValuePlaceHolders = '?, ?';
        return $specificValuePlaceHolders;
    }

    public function getSpecificValues()
    {
        $specificValueArray = array('DVDDisc', $this->getSize());
        return $specificValueArray;
    }

    public function getAttributeUnit()
    {
        return self::ATTRIBUTE_UNIT;
    }

    public function getSize()
    {
        return $this->size;
    }
    
    public function setSize($size)
    {
        $this->size = $size;
    }
}