<?php
namespace app\productClasses;
abstract class ProductItem
{


    private $sku;
    private $name;
    private $priceUsd;
    
    abstract function getAttributeValue();
    abstract function getAttributeName();
    abstract function getAttributeUnit();

    abstract static function createItem();
    
    public function getValuePlaceHolders()
    {
        $placeHolders = '?, ?, ?';
        $placeHolders = $placeHolders . ', ' . $this->getSpecificValuePlaceHolders();
        return $placeHolders;
    }
    abstract function getSpecificValuePlaceHolders();

    public function getValues()
    {
        $valueArray = array($this->getSku(), $this->getName(), $this->getPriceUsd());
        $specificValueArray = $this->getSpecificValues();
        foreach ($specificValueArray as $specificValue) {
            array_push($valueArray, $specificValue);
        }
        return $valueArray;

    }
    abstract function getSpecificValues();

    public function getColumnNames()
    {
        $columnNames = 'sku, name, price_usd, class';
        $columnNames = $columnNames . ', ' . $this->getSpecificColumnNames();
        return $columnNames;
    }
    abstract function getSpecificColumnNames();

    public function __construct($sku, $name, $priceUsd)
    {
        $this->setSku($sku);
        $this->setName($name);
        $this->setPriceUsd($priceUsd);
    }
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPriceUsd($priceUsd)
    {
        $this->priceUsd = $priceUsd;
    }

   
    public function getSku()
    {
        return $this->sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPriceUsd()
    {
        return $this->priceUsd;
    }

}