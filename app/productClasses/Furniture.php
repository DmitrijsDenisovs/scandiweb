<?php
namespace app\productClasses;
use app\productClasses\ProductItem;

class Furniture extends ProductItem
{
    private const ATTRIBUTE_NAME = 'Dimensions';
    private const ATTRIBUTE_UNIT = '';
    private $height;
    private $width;
    private $length;

    public function __construct($sku, $name, $priceUsd, $size, $weight, $height, $width, $length)
    {
        parent::__construct($sku, $name, $priceUsd);
        $this->setHeight($height);
        $this->setWidth($width);
        $this->setLength($length);
    }
    static function createItem(){
        $newItem = new Furniture($_POST['sku'], $_POST['name'], $_POST['price'], null, null, $_POST['height'], 
            $_POST['width'], $_POST['length']);
        unset($_POST['sku']);
        unset($_POST['name']);
        unset($_POST['price']);
        unset($_POST['height']);
        unset($_POST['width']);
        unset($_POST['length']);
        return $newItem;
    }

    public function getAttributeName()
    {
        return self::ATTRIBUTE_NAME;
    }

    public function getAttributeValue()
    {
        $dimensions = (string) $this->getHeight() . 'x' . $this->getWidth() . 'x' . $this->getLength();
        return $dimensions;
    }

    public function getSpecificColumnNames()
    {
        $specificColumnNames = 'height, width, length';
        return $specificColumnNames;
    }

    public function getSpecificValues()
    {
        $specificValueArray = array('Furniture', $this->getHeight(), $this->getWidth(), $this->getLength());
        return $specificValueArray;
    }

    public function getSpecificValuePlaceHolders()
    {
        $specificValuePlaceHolders = '?, ?, ?, ?';
        return $specificValuePlaceHolders;
    }

    public function getAttributeUnit()
    {
        return self::ATTRIBUTE_UNIT;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getLength()
    {
        return $this->length;
    }
}