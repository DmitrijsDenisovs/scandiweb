$( document ).ready(function() { 
    $("#productType").prop("selectedIndex", -1);
    
    $(".Furniture").hide();
    $(".Book").hide();
    $(".DVDDisc").hide();
    
    var type = "";
    
    $("#productType").on("input",function(){
        type = $(this).val();
        switch (type){
            case "DVDDisc":
                $(".Furniture").hide();
                $(".Book").hide();
                $(".DVDDisc").show();
                break;
            case "Furniture":
                $(".Furniture").show();
                $(".Book").hide();
                $(".DVDDisc").hide();
                break;
            case "Book":
                $(".Furniture").hide();
                $(".Book").show();
                $(".DVDDisc").hide();
                break;
            default:
                break;
        }
    })

    $.validator.addMethod('minStrict', function (value, el, param) {
        return value > param;
    });
    $("#product_form").validate({
        rules: {
            sku: "required",
            name: "required",
            price: { 
                required: true,
                number: true,
                minStrict: 0.00
            },
            productType: "required",
            size: {
                required: {
                    depends: function(){
                    if(type == "DVDDisc")
                        return true;
                    else
                        return false;
                    }
                },
                number: true,
                minStrict: 0
            },
            weight: {
                required: {
                    depends: function(){
                    if(type == "Book")
                        return true;
                    else
                        return false;
                    }
                },
                number: true,
                min: 1
            },
            height: {
                required: {
                    depends: function(){
                    if(type == "Furniture")
                        return true;
                    else
                        return false;
                    }
                },
                number: true,
                min: 1
            },
            width: {
                required: {
                    depends: function(){
                    if(type == "Furniture")
                        return true;
                    else
                        return false;
                    }
                },
                number: true,
                min: 1
            },
            length: {
                required: {
                    depends: function(){
                    if(type == "Furniture")
                        return true;
                    else
                        return false;
                    }
                },
                number: true,
                min: 1
            }
        },
        messages: {
            sku: "Please, submit required data",
            name: "Please, submit required data",
            price: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
            productType: "Please, submit required data",
            size: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
            weight: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
            height: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
            width: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
            length: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
                minStrict: "Please, provide positive value"
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});



    
    
    
    
    
    
    



    
